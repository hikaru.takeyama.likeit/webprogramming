package contoroller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserUpdateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
		User user = userDao.findById(id);
		request.setAttribute("user", user);

		RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
		dispatchar.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String userName = request.getParameter("userName");
		String birthday = request.getParameter("birthday");

//		未入力がある場合

		if (password.equals("") || password2.equals("") || userName.equals("") || birthday.equals("")) {
			request.setAttribute("errMsg", "未入力の箇所があります。");
			request.setAttribute("userName", userName);
			request.setAttribute("birthday", birthday);
			RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			dispatchar.forward(request, response);
			return;
		}

//		パスワードが一致しない

		if (password.equals(password2) == false) {
			request.setAttribute("errMsg2", "パスワードが一致していません。");
			request.setAttribute("userName", userName);
			request.setAttribute("birthday", birthday);
			RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			dispatchar.forward(request, response);
			return;
		}

//		パスワードを暗号化する

		UserDao userDao = new UserDao();
		String encryptedPassword = userDao.encryptPassword(password);



//		更新を実行してユーザ一覧へ遷移

		userDao.updateUserInfo(id, encryptedPassword, userName, birthday);
		ArrayList<User> userList = userDao.findAll();
		request.setAttribute("userList", userList);
		RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
		dispatchar.forward(request, response);



	}

}
