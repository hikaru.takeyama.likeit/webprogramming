package contoroller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserDeleteServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
		User user = userDao.findById(id);
		request.setAttribute("user", user);

		RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userDelete.jsp");
		dispatchar.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
		userDao.deleteUserInfo(id);
		response.sendRedirect("UserListServlet");
	}

}
