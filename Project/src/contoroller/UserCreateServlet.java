package contoroller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserCreateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		フォームから送られたリクエストパラメータを取得する

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String userName = request.getParameter("userName");
		String birthday = request.getParameter("birthday");

//		未入力がないかチェックする

		if (loginId.equals("") || password.equals("") || password2.equals("") || userName.equals("") || birthday.equals("")) {
			request.setAttribute("errMsg", "未入力の箇所があります。");
			RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
			dispatchar.forward(request, response);
			return;
		}

//		ログインIDの重複をチェックする

		UserDao userDao = new UserDao();
		User user = userDao.findByloginID(loginId);
		if (user != null) {
			request.setAttribute("errMsg2", "入力されたログインIDはすでに使用されています。");
			RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
			dispatchar.forward(request, response);
			return;
		}

//		パスワードが一致しているかをチェックする

		if (password.equals(password2) == false) {
			request.setAttribute("errMsg3", "パスワードが一致していません。");
			RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
			dispatchar.forward(request, response);
			return;
		}

//		パスワードを暗号化する

		String encryptedPassword = userDao.encryptPassword(password);

//		DBにINSERTしてユーザ一覧画面にフォーワード

		userDao.createUser(loginId, encryptedPassword, userName, birthday);
		ArrayList<User> userList = userDao.findAll();
		request.setAttribute("userList", userList);
		RequestDispatcher dispatchar = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
		dispatchar.forward(request, response);


	}

}
