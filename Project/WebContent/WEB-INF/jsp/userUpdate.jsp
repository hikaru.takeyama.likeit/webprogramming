<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザ情報更新</title>
		<link rel="stylesheet" href="css/userUpdate.css">
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/footer.css">
	</head>
	<body>
		<header>
			<div>
				<p>${userInfo.name} さん</p>
				<a href="LogoutServlet">ログアウト</a>
			</div>
		</header>
		<div class="update-form">
			<h1>ユーザ情報更新</h1>
			<form action="UserUpdateServlet" method="post">
				ログインID　　${user.loginId}<br><br>
				パスワード　　<input type="password" name="password"><br><br>
				パスワード(確認)　　<input type="password" name="password2"><br><br>
				ユーザ名　　<input type="text" name="userName" value="${user.name}"><br><br>
				生年月日　　<input type="date" name="birthday" value="${user.birthDate}"><br><br>
				<input type="hidden" name="id" value="${user.id}">
				<input type="submit" value="更新">
			</form>
		</div>
		<footer>
			<a href="UserListServlet">戻る</a>
		</footer>
	</body>
</html>