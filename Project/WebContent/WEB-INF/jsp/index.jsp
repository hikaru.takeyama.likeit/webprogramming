<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
	<link rel="stylesheet" href="css/index.css">

</head>
<body>
	<div>
		<h1>ログイン画面</h1>
	</div>
	<c:if test="${errMsg != null}" >
	    <div class="errorMessage">
		  ${errMsg}
		</div>
	</c:if>
	<div>
		<br><br><br>
		<form action="LoginServlet" method="post">
			ログインID　　<input type="text" name="loginId"><br><br>
			パスワード　　<input type="password" name="password"><br><br>
			<input type="submit" value="ログイン">
		</form>
	</div>
</body>
</html>