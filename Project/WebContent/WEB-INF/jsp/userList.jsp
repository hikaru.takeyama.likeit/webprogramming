<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザ一覧</title>
		<link rel="stylesheet" href="css/userList.css">
		<link rel="stylesheet" href="css/header.css">
	</head>
	<body>
		<header>
			<div>
				<p>${userInfo.name} さん</p>
				<a href="LogoutServlet">ログアウト</a>
			</div>
		</header>
		<div>
			<div>
				<h1>ユーザ一覧</h1>
				<a href="UserCreateServlet">新規登録</a>
			</div>
			<div class="search-form">
				<form action="UserSearchServlet" method="post">
					<br>
					ログインID　　<input type="text" name="loginId"><br><br>
					ユーザ名　　<input type="text" name="userName"><br><br>
					生年月日　　<input type="date" name="birthday">　～　
					<input type="date" name="birthday2"><br><br>
					<input type="submit" value="検索">

				</form>
			</div>
		</div>
		<div class="user-list">
			<table>
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
				</tr>
				<c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <td>
                     	<a href="UserDetailServlet?id=${user.loginId}">詳細</a>
                   		<c:if test="${userInfo.loginId} == admin or ${userInfo.name} == ${user.name}">
                     		<a href="UserUpdateServlet?id=${user.id}">更新</a>
                     	</c:if>
                     	<c:if test="${userInfo.loginId} == admin">
                     		<a href="UserDeleteServlet?id=${user.id}">削除</a>
                     	</c:if>
                     </td>
                   </tr>
                </c:forEach>
			</table>
		</div>
	</body>
</html>