<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザ消去確認</title>
		<link rel="stylesheet" href="css/userDelete.css">
		<link rel="stylesheet" href="css/header.css">
	</head>
	<body>
		<header>
			<div>
				<p>${userInfo.name} さん</p>
				<a href="LogoutServlet">ログアウト</a>
			</div>
		</header>
		<div>
			<h1>ユーザ消去確認</h1>
			<p>ログインID：${user.loginId}</p>
			<p>を本当に消去してもよろしいでしょうか。</p><br><br>
				<a href="UserListServlet">キャンセル</a>
			<form action="UserDeleteServlet" method="post">
				<input type="hidden" name="id" value="${user.id}">
				<input type="submit" value="OK">
			</form>
		</div>
	</body>
</html>