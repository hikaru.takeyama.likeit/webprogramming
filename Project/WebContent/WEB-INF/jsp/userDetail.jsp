<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザ情報詳細参照</title>
		<link rel="stylesheet" href="css/userDetail.css">
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/footer.css">
	</head>

	<body>
		<header>
			<div>
				<p>${userInfo.name} さん</p>
				<a href="LogoutServlet">ログアウト</a>
			</div>
		</header>
		<div>
			<h1>ユーザ情報詳細参照</h1>
		</div>
		<div>
			ログインID　　　${user.loginId}<br><br>
			ユーザ名　　　${user.name}<br><br>
			生年月日　　　${user.birthDate}<br><br>
			登録日時　　　${user.createDate}<br><br>
			更新日時　　　${user.updateDate}<br><br>
		</div>
		<footer>
			<a href="UserListServlet">戻る</a>
		</footer>
	</body>
</html>