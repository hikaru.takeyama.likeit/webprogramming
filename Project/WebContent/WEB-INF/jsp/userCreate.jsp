<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザ新規登録</title>
		<link rel="stylesheet" href="css/userCreate.css">
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/footer.css">
	</head>
	<body>
		<header>
			<div>
				<p>${userInfo.name} さん</p>
				<a href="LogoutServlet">ログアウト</a>
			</div>
		</header>
		<div class="user-create-form">
			<h1>ユーザ新規登録</h1>
			<c:if test="${errMsg != null}" >
				<div class="errorMessage">
					${errMsg}
				</div>
			</c:if>
			<c:if test="${errMsg2 != null}" >
				<div class="errorMessage">
					${errMsg2}
				</div>
			</c:if>
			<c:if test="${errMsg3 != null}" >
				<div class="errorMessage">
					${errMsg3}
				</div>
			</c:if>
			<form action="UserCreateServlet" method="post">
				ログインID　　<input type="text" name="loginId"><br><br>
				パスワード　　<input type="password" name="password"><br><br>
				パスワード(確認)　　<input type="password" name="password2"><br><br>
				ユーザ名　　<input type="text" name="userName"><br><br>
				生年月日　　<input type="date" name="birthday"><br><br>
				<input type="submit" value="登録">
			</form>
		</div>
		<footer>
			<a href="UserListServlet">戻る</a>
		</footer>
	</body>
</html>